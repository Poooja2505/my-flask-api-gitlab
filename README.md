# My python APi

### Steps:
* python3 -m venv venv ==> create the venv env
* source venv/bin/activate ==> to activate the venv
* any pip command will not be installed in this new env
* to deactivate the venv
* install flask: pip install flask

* create the app.py flask API

* export FLASK_APP=app
* export FLASK_ENV=development 

* how to run: python app.py

* capture the requirement in txt == > pip freeze > requirements.txt

* Add Docker file

* run docker build and tag and run and push
docker build -t my-flask-api:v1 .
docker images | grep "flask"
docker run -p5001:5000 my-flask-api 
docker tag my-flask-api:v1  333743/my-flask-api:v1
docker push 333743/my-flask-api:v1

* testing:
    * to run python test:  python -m unittest test_app.py
    * coverage run --omit="test_*" -m unittest test_app.py
    * coverage html 
    * this will generate an html report under: htmlcov


* can be seen here: https://hub.docker.com/r/333743/my-flask-api

* Reference: https://dev.to/codemaker2015/build-and-deploy-flask-rest-api-on-docker-25mf


KUBECTL_HELM_IMAGE: dtzar/helm-kubectl:3.10